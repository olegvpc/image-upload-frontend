import './App.css';
import { useState } from 'react';

import Upload from './Upload';
import DrawRectange from './DrawRectange';

function App() {
  const [images, setImages] = useState([]);

  return (
    <div className='app'>
        <DrawRectange
          images={images}
          setImages={setImages}
        />
       <Upload
        setImages={setImages}
      />
    </div>
  );
}

export default App;
