import './DrawPoints.css';
import { useEffect, useRef } from 'react';
import picture from '../../images/plate.jpg'

const DrawPoints = () => {
    const canvasRef = useRef(null);
    const contextRef = useRef(null);

    const canvasOffSetX = useRef(null);
    const canvasOffSetY = useRef(null);
    const refX = useRef(null);
    const refY = useRef(null);

    useEffect(() => {
        const canvas = canvasRef.current;
        canvas.width = 500;
        canvas.height = 500;

        const canvasOffSet = canvas.getBoundingClientRect();
        canvasOffSetX.current = canvasOffSet.left;
        canvasOffSetY.current = canvasOffSet.top;

        const context = canvas.getContext("2d");
        // context.lineCap = "round";
        // context.fillStyle = "rgba(10, 10, 100, 0.5)";
        // context.strokeStyle = "rgba(10, 10, 100, 0.5)";
        // context.lineWidth = 5;
        contextRef.current = context;
        const img = new Image()
        img.src = picture
        img.onload = function() {
            const pattern = context.createPattern(img, "no-repeat");
            context.fillStyle = pattern;
            context.fillRect(0, 0, 500, 500);
            context.strokeRect(0, 0, 500, 500);
        }
            // context.drawImage(img, 0, 0, 500, 500)
    }, []);

    function handleClick(e) {
        e.preventDefault();
        e.stopPropagation();

        refX.current = e.clientX - canvasOffSetX.current
        refY.current = e.clientY - canvasOffSetY.current;

        contextRef.current.fillStile = 'red';
        contextRef.current.beginPath();
        contextRef.current.arc(refX.current, refY.current, 2, 0, Math.PI*2)
        console.log("POINT", refX.current, refY.current)
        console.log("CANVAS-OFF-SET", canvasOffSetX.current, canvasOffSetY.current)
        contextRef.current.fill()
        contextRef.current.stroke()
    }

    return (
        <div className='canvas-container'>
            <canvas className="canvas-frame"
                ref={canvasRef}
                onClick={handleClick}
                // onMouseDown={startDrawingRectangle}
                // onMouseMove={drawRectangle}
                // onMouseUp={stopDrawingRectangle}
                // onMouseLeave={stopDrawingRectangle}
            >
            </canvas>
        </div>
    )
}

export default DrawPoints;