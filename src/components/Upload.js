import './Upload.css';

function Upload({ setImages }) {
  function handleInput(e) {
    // console.log(e.target.files[0]);
    // File {name: 'Водит Ткач О-1.jpeg', lastModified: 1616083686000, :08:06
    // GMT+0700 (Индокитай)...}

    const fileList = e.target.files;
    const fileData = fileList[0]; // Save Object File in the List useState
    setImages((prev) => [fileData, ...prev]);
  }

  return (
    <div className='upload__container'>
      <div className='upload__title'>
        <h2>Upload Images</h2>
        <div className='server-message'>here will be server response (optional)</div>
      </div>

      <div className='uppload__input-container'>
        <p>Drag & drop photos here or <span className="browse">Browse</span></p>
        <input type='file'
               className='upload__input-file'
               // multiple='multiple'
               accept='image/jpeg, image/png, image/jpg'
               onChange={handleInput}
        />
      </div>
    </div>
  );
}

export default Upload;
