import './DrawRectange.css';
import { useEffect, useRef, useState } from 'react';
import noPicture from '../images/noimage.png'

const DrawRectange = ({ images, setImages }) => {
  // console.log(images) // all added images
  let imageSrc = noPicture
  if(images.length !== 0 ) {
    imageSrc = URL.createObjectURL(images[0]); // last added image
  }

  const canvasRef = useRef(null);
  const contextRef = useRef(null);

  const [isDrawing, setIsDrawing] = useState(false);

  const canvasOffSetX = useRef(null);
  const canvasOffSetY = useRef(null);
  const startX = useRef(null);
  const startY = useRef(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    canvas.width = 480;
    canvas.height = 360;

    const context = canvas.getContext('2d');
    context.lineCap = 'round';
    context.strokeStyle = 'rgba(100, 100, 100, 0.8)';
    context.fillStyle = 'rgba(100, 100, 100, 0.8)';
    context.lineWidth = 1;
    contextRef.current = context;

    const canvasOffSet = canvas.getBoundingClientRect();
    canvasOffSetX.current = canvasOffSet.left;
    canvasOffSetY.current = canvasOffSet.top;
  }, []);

  const startDrawingRectangle = ({ nativeEvent }) => {
    nativeEvent.preventDefault();
    nativeEvent.stopPropagation();

    startX.current = nativeEvent.clientX - canvasOffSetX.current;
    startY.current = nativeEvent.clientY - canvasOffSetY.current;
    console.log('START DRAWING', startX.current, startY.current);
    setIsDrawing(true);
  };

  const drawRectangle = ({ nativeEvent }) => {
    if (!isDrawing) {
      return;
    }
    nativeEvent.preventDefault();
    nativeEvent.stopPropagation();

    const newMouseX = nativeEvent.clientX - canvasOffSetX.current;
    const newMouseY = nativeEvent.clientY - canvasOffSetY.current;

    const rectWidht = newMouseX - startX.current;
    const rectHeight = newMouseY - startY.current;

    contextRef.current.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);
    console.log(newMouseX, newMouseY);
    contextRef.current.strokeRect(startX.current, startY.current, rectWidht, rectHeight);
    contextRef.current.fillRect(startX.current, startY.current, rectWidht, rectHeight)
  };

  const stopDrawingRectangle = () => {
    setIsDrawing(false);
  };
  function deleteImage() {
    setImages([]);
    contextRef.current.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height); // Crearing canvas
  }

  return (
    <div className='canvas__container'>
      <button className='image__delete-btn' onClick={deleteImage}>&times;</button>
      <canvas className='canvas__frame'
              ref={canvasRef}
              style={{ backgroundImage: `url(${imageSrc})`,
                objectFit: "cover",
                backgroundPosition: '50% 50%',
                backgroundSize: '100%',
                backgroundRepeat: 'no-repeat'}}
              onMouseDown={startDrawingRectangle}
              onMouseMove={drawRectangle}
              onMouseUp={stopDrawingRectangle}
              onMouseLeave={stopDrawingRectangle}>
      </canvas>
    </div>
  );
};

export default DrawRectange;
